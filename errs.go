package tpl

import "gitlab.com/leolab/go/errs"

const (
	ErrTemplateError errs.ErrCode = "ErrTemplateError"
	ErrFileError     errs.ErrCode = "ErrFileError"
)
