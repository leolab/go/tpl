package tpl

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"strings"
	"text/template"
	"time"

	"gitlab.com/leolab/go/errs"
	"gitlab.com/leolab/go/leotools"
)

type TPL struct {
	store   fs.FS
	funcMap template.FuncMap
}

func NewTPL(files fs.FS) *TPL {
	t := &TPL{
		store: files,
		funcMap: template.FuncMap{
			"inc":       func(i int) int { return i + 1 },
			"dec":       func(i int) int { return i - 1 },
			"date":      func(f string, t time.Time) string { return t.Format(f) },
			"timestamp": func() string { return fmt.Sprint(time.Now().Unix()) },
			"objDump": func(d interface{}) string {
				s, e := json.MarshalIndent(d, "", "\t")
				if e != nil {
					return fmt.Sprintf("%+v", d)
					//return e.Error()
				}
				return string(s)
			},
		},
	}
	t.funcMap["include"] = func(fName string, data map[string]interface{}) string {
		return string(t.Content(fName, data))
	}
	return t
}

//Добавить функцию
func (t *TPL) AddTag(name string, v interface{}) (err *errs.Err) {
	t.funcMap[name] = v
	return nil
}

//Основной метод
func (t *TPL) Content(fName string, data map[string]interface{}) []byte {
	//var err *errs.Err
	if data == nil {
		data = make(map[string]interface{})
	}
	f, e := t.store.Open(fName + ".html")
	if e != nil {
		return []byte(errs.RaiseError(ErrFileError, "(1) "+e.Error()).Error())
	}
	defer f.Close()
	pg, e := io.ReadAll(f)
	if e != nil {
		return []byte(errs.RaiseError(ErrFileError, "(2)"+e.Error()).Error())
	}

	//Page definition
	d := t.loadDef(fName)
	if d != nil {
		if _, ok := data["Def"]; ok {
			data["Def"] = leotools.JoinMap(data["Def"].(map[string]interface{}), d)
		} else {
			data["Def"] = d
		}
	}

	//Page scripts
	if s := t.loadJS(fName); s != "" {
		if _, ok := data["Scripts"].(string); ok {
			data["Scripts"] = data["Scripts"].(string) + s
		} else {
			data["Scripts"] = s
		}
	}

	if pg[0] == '#' {
		b := bytes.NewBuffer(pg)
		s, e := b.ReadString('\n')
		if e != nil {
			return []byte(errs.RaiseError(ErrTemplateError, e.Error()).Error())
		}
		sa := strings.Split(strings.Trim(s, "#\n\r"), " ")
		switch strings.ToLower(sa[0]) {
		case "extends":
			data["Content"] = string(t.Parse(b.Bytes(), data))
			return t.Content(strings.Trim(sa[1], "\""), data)
		default:
			return []byte(errs.RaiseError(ErrTemplateError, "Error parse template '"+fName+"': Unknown argument: "+sa[0]).Error())
		}
	}
	return t.Parse(pg, data)
}

func (t *TPL) Parse(tdata []byte, data interface{}) []byte {
	tpl, e := template.New("template").Funcs(t.funcMap).Parse(string(tdata))
	if e != nil {
		return []byte(errs.RaiseError(ErrTemplateError, e.Error()).Error())
	}
	var tw bytes.Buffer
	e = tpl.Execute(&tw, data)
	if e != nil {
		return []byte(errs.RaiseError(ErrTemplateError, e.Error(), data).Error())
	}
	return tw.Bytes()
}

func (t *TPL) loadDef(page string) (def map[string]interface{}) {
	if f, e := t.store.Open(page + ".def.json"); e == nil {
		c, e := io.ReadAll(f)
		if e == nil {
			if e := json.Unmarshal(c, &def); e == nil {
				return def
			} else {
				errs.RaiseError(ErrFileError, e.Error())
			}
		} else {
			errs.RaiseError(ErrFileError, e.Error())
		}
	}
	return nil
}

func (t *TPL) loadJS(page string) string {
	if f, err := t.store.Open(page + ".js"); err == nil {
		c, e := io.ReadAll(f)
		if e == nil {
			return "\n<script>\n" + string(c) + "\n</script>\n"
		} else {
			errs.RaiseError(ErrFileError, e.Error())
		}
	}
	return ""
}
