module gitlab.com/leolab/go/tpl

go 1.17

require (
	github.com/davecgh/go-spew v1.1.1
	gitlab.com/leolab/go/errs v0.1.1
	gitlab.com/leolab/go/leotools v0.4.8
)

require gitlab.com/leolab/go/replacer v1.1.1 // indirect
